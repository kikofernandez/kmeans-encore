type Pair a b = (a, b)
data Centroid a = Centroid { color :: a,
                             point :: Pair a a
                           }

points = undefined --[(1,1), (2,2), (3,3)]

-- euclidian distance of two vectors
distance :: Floating a => Pair a a -> Pair a a -> a
distance (x1, x2) (y1, y2) =  sqrt $ ((x1-y1)^2) + ((x2-y2)^2)

getCentroids :: [Centroid a]
getCentroids = undefined

assignPointToCentroid :: [Centroid a] -> (a, a) -> (a, (a, a))
assignPointToCentroid = undefined

updateCentroid = undefined

mergePoints :: [(a, (a, a))] -> [(a, [Pair a a])]
mergePoints = undefined

runKmeans :: [Centroid a] -> [(a, [Pair a a])]
runKmeans centroids =
  let points' = map (assignPointToCentroid centroids) points
      centroids' = updateCentroid centroids in
  if centroids == centroids' then runKmeans centroids'
  else mergePoints points'


-- kmeans :: [Pair a a] -> [(a, [Pair a a])]
-- kmeans =
--   let centroids = getCentroids in
--   runKmeans centroids


instance Eq (Centroid a) where
  (==) x y = True


main = undefined
